import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemComponent } from './component/item/item.component';
import { ItemListPageComponent } from './page/item-list-page/item-list-page.component';
import { EditItemPageComponent } from './page/edit-item-page/edit-item-page.component';
import { MainComponent } from './main/main.component';
import { PageNotFoundComponent } from './page/page-not-found/page-not-found.component';
import { routes } from './app-routing.module';
import { AddItemFormComponent } from './form/add-item-form/add-item-form.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { FilterCategoryComponent } from './filter/filter-category/filter-category.component';
import { FilterNameComponent } from './filter/filter-name/filter-name.component';
import { LoginComponent } from './form/login/login.component';
import { RegisterComponent } from './form/register/register.component';
import { AboutPageComponent } from './page/about-page/about-page.component';
import { ContactPageComponent } from './page/contact-page/contact-page.component';
import { ItemDetailsPageComponent } from './page/item-details-page/item-details-page.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    ItemListPageComponent,
    EditItemPageComponent,
    MainComponent,
    PageNotFoundComponent,
    AddItemFormComponent,
    NavbarComponent,
    FilterCategoryComponent,
    FilterNameComponent,
    LoginComponent,
    RegisterComponent,
    AboutPageComponent,
    ContactPageComponent,
    ItemDetailsPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      routes,
      { enableTracing: true } // <-- debugging purposes only
    ),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
