import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { AboutPageComponent } from './page/about-page/about-page.component';
import { ContactPageComponent } from './page/contact-page/contact-page.component';
import { EditItemPageComponent } from './page/edit-item-page/edit-item-page.component';
import { ItemDetailsPageComponent } from './page/item-details-page/item-details-page.component';
import { ItemListPageComponent } from './page/item-list-page/item-list-page.component';
import { PageNotFoundComponent } from './page/page-not-found/page-not-found.component';

export const routes: Routes = [
  { path: 'items', component: ItemListPageComponent },
  { path: 'edit-item', component: EditItemPageComponent },
  { path: 'items/:id', component: ItemDetailsPageComponent },
  { path: 'about', component: AboutPageComponent },
  { path: 'contact', component: ContactPageComponent },
  { path: 'main', component: MainComponent },
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
