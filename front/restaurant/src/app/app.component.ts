import { Component } from '@angular/core';
import { AppService } from './app.service';
import { LogCredentials } from './form/login/login.component';
import { User } from './model/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  public user: User | null;
  public newUser: User;

  constructor(private appService: AppService) {
    this.user = {
      id: -1,
      username: '',
      password: '',
    };
    this.newUser = {
      id: -1,
      username: '',
      password: '',
    };
  }
  ngOnInit(): void {
    this.getUser();
  }

  login(logCredentials: LogCredentials): void {
    this.appService
      .getUser(logCredentials.username, logCredentials.password)
      .subscribe((user) => {
        if (user != undefined) {
          localStorage.setItem('user', JSON.stringify(user));
        }
        this.user = user;
      });
  }
  logout(): void {
    localStorage.removeItem('user');
    this.user = {
      id: -1,
      username: '',
      password: '',
    };
    this.getUser();
  }
  getUser(): void {
    if (localStorage.getItem('user') !== null) {
      this.user = JSON.parse(window.localStorage['user']);
    } else {
      this.user = null;
    }
  }
  register(newUser: User): void {
    this.appService.addUser(newUser).subscribe((response) => {
      console.log(response);
    });
  }
}
