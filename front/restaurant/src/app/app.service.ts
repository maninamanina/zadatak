import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from './model/category.model';
import { Item } from './model/item.model';
import { User } from './model/user.model';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  private itemURL = 'http://localhost:8080/api/items/';
  private categoryURL = 'http://localhost:8080/api/categories/';
  private userURL = 'http://localhost:8080/api/users/';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient) {}

  public getData(
    nameFilter: string,
    categoryFilter: string,
    pageNo: number
  ): Observable<Item[]> {
    return this.http.get<Item[]>(
      `${this.itemURL}?nameFilter=${nameFilter}&categoryFilter=${categoryFilter}&pageNo=${pageNo}`
    );
  }
  public getDataCategory(): Observable<Category[]> {
    return this.http.get<Category[]>(this.categoryURL);
  }
  public addItem(item: Item): Observable<Item> {
    return this.http.post<Item>(this.itemURL, item, this.httpOptions);
  }
  public removeItem(id: number): Observable<Item> {
    const url = `${this.itemURL}${id}`;
    return this.http.delete<Item>(url, this.httpOptions);
  }
  public getDataById(id: number): Observable<Item> {
    const url = `${this.itemURL}${id}`;
    return this.http.get<Item>(url, this.httpOptions);
  }
  public getUser(username: string, password: string): Observable<User> {
    return this.http.get<User>(
      `${this.userURL}login/?username=${username}&password=${password}`
    );
  }
  public addUser(user: User): Observable<User> {
    return this.http.post<User>(this.userURL, user, this.httpOptions);
  }
}
