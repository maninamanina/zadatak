import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Category } from 'src/app/model/category.model';

@Component({
  selector: 'app-filter-category',
  templateUrl: './filter-category.component.html',
  styleUrls: ['./filter-category.component.css'],
})
export class FilterCategoryComponent implements OnInit {
  public categories: Category[];
  public categoryFilter: string;
  @Output() filterEvent: EventEmitter<string> = new EventEmitter();

  constructor(private appService: AppService) {
    this.categories = [];
    this.categoryFilter = '';
  }
  ngOnInit(): void {
    this.getData();
  }
  getData(): void {
    this.appService.getDataCategory().subscribe((categories) => {
      this.categories = categories;
    });
  }
  public filterItems() {
    this.filterEvent.emit(this.categoryFilter);
  }
  public resetFilter() {
    this.categoryFilter = '';
    this.filterEvent.emit(this.categoryFilter);
  }
}
