import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filter-name',
  templateUrl: './filter-name.component.html',
  styleUrls: ['./filter-name.component.css'],
})
export class FilterNameComponent implements OnInit {
  public nameFilter: string;
  @Output() filterEvent: EventEmitter<string> = new EventEmitter();

  constructor() {
    this.nameFilter = '';
  }

  ngOnInit(): void {}

  public filterItems() {
    this.filterEvent.emit(this.nameFilter);
  }
  public resetFilter() {
    this.nameFilter = '';
    this.filterEvent.emit(this.nameFilter);
  }
}
