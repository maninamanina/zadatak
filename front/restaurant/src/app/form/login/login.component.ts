import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public logCredentials: LogCredentials;
  @Output() loginEvent: EventEmitter<LogCredentials> = new EventEmitter();

  constructor() {
    this.logCredentials = {
      username: '',
      password: '',
    };
  }

  ngOnInit(): void {}

  login(): void {
    this.loginEvent.emit(this.logCredentials);
    this.logCredentials = {
      username: '',
      password: '',
    };
  }
}
export interface LogCredentials {
  username: string;
  password: string;
}
