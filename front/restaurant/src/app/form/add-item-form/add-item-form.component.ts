import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { Category } from 'src/app/model/category.model';
import { Item } from 'src/app/model/item.model';

@Component({
  selector: 'app-add-item-form',
  templateUrl: './add-item-form.component.html',
  styleUrls: ['./add-item-form.component.css'],
})
export class AddItemFormComponent implements OnInit {
  public categories: Category[];
  @Input() newItem!: Item;
  @Output() addEvent: EventEmitter<Item> = new EventEmitter();
  @Output() resetEvent: EventEmitter<Item> = new EventEmitter();

  constructor(private appService: AppService, private router: Router) {
    this.categories = [];
    this.newItem = {
      name: '',
      price: 0,
      categoryId: -1,
    };
  }

  ngOnInit(): void {
    this.getData();
  }
  getData(): void {
    this.appService.getDataCategory().subscribe((categories) => {
      this.categories = categories;
    });
  }

  addItem(): void {
    this.addEvent.emit(this.newItem);
    this.newItem = {
      name: '',
      price: 0,
      categoryId: -1,
    };
  }
  resetItem(): void {
    this.newItem = {
      name: '',
      price: 0,
      categoryId: -1,
    };
    this.resetEvent.emit(this.newItem);
  }
}
