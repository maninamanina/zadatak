import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  public newUser: User;
  @Output() registerEvent: EventEmitter<User> = new EventEmitter();

  constructor() {
    this.newUser = {
      username: '',
      password: '',
    };
  }
  ngOnInit(): void {}

  register(): void {
    this.registerEvent.emit(this.newUser);
    this.newUser = {
      username: '',
      password: '',
    };
  }
}
