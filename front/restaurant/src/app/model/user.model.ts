interface UserInterface {
  id?: number;
  username: string;
  password: string;
}
export class User implements UserInterface {
  public id?: number;
  public username: string;
  public password: string;

  constructor(cfg: UserInterface) {
    this.id = cfg.id;
    this.username = cfg.username;
    this.password = cfg.password;
  }
}
