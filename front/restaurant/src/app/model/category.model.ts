interface CategoryInterface {
  id: number;
  name: string;
}
export class Category implements CategoryInterface {
  public id: number;
  public name: string;

  constructor(cfg: CategoryInterface) {
    this.id = cfg.id;
    this.name = cfg.name;
  }
}
