import { Category } from './category.model';

interface ItemInterface {
  id?: number;
  name: string;
  price: number;
  categoryId: number;
  categoryName?: string;
  category?: Category;
}
export class Item implements ItemInterface {
  public id?: number;
  public name: string;
  public price: number;
  public categoryId: number;
  public categoryName?: string;
  public category?: Category;

  constructor(cfg: ItemInterface) {
    this.id = cfg.id;
    this.name = cfg.name;
    this.price = cfg.price;
    this.categoryId = cfg.categoryId;
    this.categoryName = cfg.categoryName;
    this.category = cfg.category;
  }
}
