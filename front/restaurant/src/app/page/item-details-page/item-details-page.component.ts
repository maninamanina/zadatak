import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { Item } from 'src/app/model/item.model';

@Component({
  selector: 'app-item-details-page',
  templateUrl: './item-details-page.component.html',
  styleUrls: ['./item-details-page.component.css'],
})
export class ItemDetailsPageComponent implements OnInit {
  public item: Item;
  public id: number;
  private sub: any;
  isDataAvailable: boolean = false;

  constructor(private route: ActivatedRoute, private appService: AppService) {
    this.id = -1;
    this.item = {
      name: '',
      price: 0,
      categoryId: -1,
    };
  }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe((params) => {
      this.isDataAvailable = false;
      this.id = +params['id'];
      this.getItem(this.id);
    });
  }

  getItem(id: number): void {
    this.appService.getDataById(this.id).subscribe((item: Item) => {
      if (item.category !== undefined) {
        this.item = {
          id: item.id,
          name: item.name,
          price: item.price,
          categoryId: item.category.id,
          categoryName: item.category.name,
          category: item.category,
        };
      }
    });
  }
}
