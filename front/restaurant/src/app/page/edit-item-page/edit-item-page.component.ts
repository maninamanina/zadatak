import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Item } from 'src/app/model/item.model';

@Component({
  selector: 'app-edit-item-page',
  templateUrl: './edit-item-page.component.html',
  styleUrls: ['./edit-item-page.component.css'],
})
export class EditItemPageComponent implements OnInit {
  public items: Item[];
  public newItem: Item;
  public nameFilter: string;
  public categoryFilter: string;
  public pageNo: number;

  constructor(private appService: AppService) {
    this.items = [];
    this.newItem = {
      name: '',
      price: 0,
      categoryId: -1,
    };
    this.nameFilter = '';
    this.categoryFilter = '';
    this.pageNo = 0;
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.appService
      .getData(this.nameFilter, this.categoryFilter, this.pageNo)
      .subscribe((items) => {
        this.items = items;
      });
  }

  addItem(item: Item): void {
    this.appService.addItem(item).subscribe((response) => {
      this.getData();
    });
  }
  resetItem(item: Item): void {
    this.newItem = item;
  }
  removeItem(id: number): void {
    this.appService.removeItem(id).subscribe((item: Item) => {
      this.getData();
    });
  }

  editItem(id: number): void {
    this.appService.getDataById(id).subscribe((item: Item) => {
      if (item.category !== undefined) {
        this.newItem = {
          id: item.id,
          name: item.name,
          price: item.price,
          categoryId: item.category.id,
          categoryName: item.category.name,
          category: item.category,
        };
      }
    });
  }
  filterItem(nameFilter: string) {
    this.nameFilter = nameFilter;
    this.getData();
  }
  resetFilter() {
    this.nameFilter = '';
    this.getData();
  }
  prev() {
    this.pageNo = this.pageNo - 1;
    this.getData();
  }
  next() {
    this.pageNo = this.pageNo + 1;
    this.getData();
  }
}
