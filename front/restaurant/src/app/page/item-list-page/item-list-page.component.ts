import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Item } from 'src/app/model/item.model';

@Component({
  selector: 'app-item-list-page',
  templateUrl: './item-list-page.component.html',
  styleUrls: ['./item-list-page.component.css'],
})
export class ItemListPageComponent implements OnInit {
  public items: Item[];
  public nameFilter: string;
  public categoryFilter: string;
  public pageNo: number;

  constructor(private appService: AppService) {
    this.items = [];
    this.categoryFilter = '';
    this.nameFilter = '';
    this.pageNo = 0;
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.appService
      .getData(this.nameFilter, this.categoryFilter, this.pageNo)
      .subscribe((items: Item[]) => {
        this.items = items;
      });
  }
  filterItem(categoryFilter: string) {
    this.categoryFilter = categoryFilter;
    this.getData();
  }
  resetFilter() {
    this.categoryFilter = '';
    this.getData();
  }
  prev() {
    this.pageNo = this.pageNo - 1;
    this.getData();
  }
  next() {
    this.pageNo = this.pageNo + 1;
    this.getData();
  }
}
