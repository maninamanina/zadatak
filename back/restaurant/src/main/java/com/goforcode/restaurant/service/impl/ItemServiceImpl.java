package com.goforcode.restaurant.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.goforcode.restaurant.model.Item;
import com.goforcode.restaurant.repository.ItemRepository;
import com.goforcode.restaurant.service.ItemService;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemRepository itemRepository;

	@Override
	public Page<Item> findAll(Integer pageNo) {

		return itemRepository.findAll(PageRequest.of(pageNo, 4));
	}

	@Override
	public Page<Item> findByName(String nameFilter, Integer pageNo) {

		return itemRepository.findByNameContains(nameFilter, PageRequest.of(pageNo, 4));
	}

	@Override
	public Item save(Item item) {

		return itemRepository.save(item);
	}

	@Override
	public Item findById(Long id) {

		return itemRepository.findById(id).get();
	}

	@Override
	public void deleteById(Long id) {

		itemRepository.deleteById(id);

	}

	@Override
	public Page<Item> findByCategoryName(String categoryName, Integer pageNo) {

		return itemRepository.findByCategoryName(categoryName, PageRequest.of(pageNo, 4));
	}

	@Override
	public Page<Item> findByNameAndCategoryName(String nameFilter, String categoryName, Integer pageNo) {

		return itemRepository.findByNameContainsAndCategoryName(nameFilter, categoryName, PageRequest.of(pageNo, 4));
	}
}
