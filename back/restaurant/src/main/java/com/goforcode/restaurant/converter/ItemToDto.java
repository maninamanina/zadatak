package com.goforcode.restaurant.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.goforcode.restaurant.dto.ItemDto;
import com.goforcode.restaurant.model.Item;

@Component
public class ItemToDto implements Converter<Item, ItemDto> {

	@Override
	public ItemDto convert(Item i) {

		ItemDto dto = new ItemDto();

		dto.setId(i.getId());
		dto.setName(i.getName());
		dto.setPrice(i.getPrice());

		dto.setCategoryId(i.getCategory().getId());
		dto.setCategoryName(i.getCategory().getName());

		return dto;
	}

	public List<ItemDto> convert(List<Item> list) {
		List<ItemDto> dto = new ArrayList<ItemDto>();
		for (Item i : list) {
			dto.add(convert(i));
		}
		return dto;
	}

}
