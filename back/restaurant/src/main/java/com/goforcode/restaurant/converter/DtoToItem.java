package com.goforcode.restaurant.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.goforcode.restaurant.dto.ItemDto;
import com.goforcode.restaurant.model.Item;
import com.goforcode.restaurant.service.CategoryService;
import com.goforcode.restaurant.service.ItemService;

@Component
public class DtoToItem implements Converter<ItemDto, Item> {

	@Autowired
	private ItemService itemService;
	@Autowired
	private CategoryService categoryService;

	@Override
	public Item convert(ItemDto dto) {
		Item i;
		if (dto.getId() == null) {
			i = new Item();
		} else {
			i = itemService.findById(dto.getId());
		}
		i.setName(dto.getName());
		i.setPrice(dto.getPrice());
		i.setCategory(categoryService.findById(dto.getCategoryId()));

		return i;
	}

}
