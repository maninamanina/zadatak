package com.goforcode.restaurant.service;

import java.util.List;

import com.goforcode.restaurant.model.Category;

public interface CategoryService {

	public List<Category> findAll();

	public Category findById(Long id);

}
