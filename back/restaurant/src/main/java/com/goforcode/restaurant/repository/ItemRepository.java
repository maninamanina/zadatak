package com.goforcode.restaurant.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.goforcode.restaurant.model.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

	public List<Item> findByNameContains(String name);

	public Page<Item> findAll(Pageable pageable);

	public Page<Item> findByNameContains(String name, Pageable pageable);

	public Page<Item> findByCategoryName(String categoryName, Pageable pageable);

	public Page<Item> findByNameContainsAndCategoryName(String name, String categoryName, Pageable pageable);

}
