package com.goforcode.restaurant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.goforcode.restaurant.model.User;
import com.goforcode.restaurant.service.UserService;

@RestController
@RequestMapping(value = "api/users")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping
	public ResponseEntity<User> create(@RequestBody User user) {

		User save = userService.save(user);

		return new ResponseEntity<>(save, HttpStatus.CREATED);
	}

	@GetMapping(value = "/login")
	public ResponseEntity<User> findByUsernameAndPassword(
			@RequestParam(required = true, value = "username") String username,
			@RequestParam(required = true, value = "password") String password) {
		User user = userService.findByUsernameAndPassword(username, password);
		if (user != null) {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
