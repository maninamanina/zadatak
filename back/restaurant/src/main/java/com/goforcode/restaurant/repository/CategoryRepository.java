package com.goforcode.restaurant.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.goforcode.restaurant.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
