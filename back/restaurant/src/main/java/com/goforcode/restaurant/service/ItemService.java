package com.goforcode.restaurant.service;

import org.springframework.data.domain.Page;

import com.goforcode.restaurant.model.Item;

public interface ItemService {

	public Page<Item> findAll(Integer pageNo);

	public Page<Item> findByName(String nameFilter, Integer pageNo);

	public Item save(Item item);

	public Item findById(Long id);

	public void deleteById(Long id);

	public Page<Item> findByCategoryName(String categoryName, Integer pageNo);

	public Page<Item> findByNameAndCategoryName(String nameFilter, String categoryName, Integer pageNo);

}
