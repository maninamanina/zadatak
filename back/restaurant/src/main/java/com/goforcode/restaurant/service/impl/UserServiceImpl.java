package com.goforcode.restaurant.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.goforcode.restaurant.model.User;
import com.goforcode.restaurant.repository.UserRepository;
import com.goforcode.restaurant.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> findAll() {

		return userRepository.findAll();
	}

	@Override
	public User save(User user) {

		return userRepository.save(user);
	}

	@Override
	public User findByUsernameAndPassword(String username, String password) {

		return userRepository.findByUsernameAndPassword(username, password);
	}
}
