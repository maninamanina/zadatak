package com.goforcode.restaurant.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.goforcode.restaurant.model.Category;
import com.goforcode.restaurant.repository.CategoryRepository;
import com.goforcode.restaurant.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public List<Category> findAll() {

		return categoryRepository.findAll();
	}

	@Override
	public Category findById(Long id) {

		return categoryRepository.findById(id).get();
	}
}
