package com.goforcode.restaurant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.goforcode.restaurant.converter.DtoToItem;
import com.goforcode.restaurant.converter.ItemToDto;
import com.goforcode.restaurant.dto.ItemDto;
import com.goforcode.restaurant.model.Item;
import com.goforcode.restaurant.service.ItemService;

@RestController
@RequestMapping(value = "api/items")
@CrossOrigin(origins = "http://localhost:4200")
public class ItemController {

	@Autowired
	private ItemService itemService;

	@Autowired
	private DtoToItem toItem;

	@Autowired
	private ItemToDto toDto;

	@GetMapping
	public ResponseEntity<List<Item>> findAll(@RequestParam(required = false, value = "nameFilter") String nameFilter,
			@RequestParam(required = false, value = "categoryFilter") String categoryName,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		Page<Item> items;

		if (categoryName != "" && nameFilter == "") {
			items = itemService.findByCategoryName(categoryName, pageNo);
		} else if (categoryName == "" && nameFilter != "") {
			items = itemService.findByName(nameFilter, pageNo);
		} else if (categoryName != "" && nameFilter != "") {
			items = itemService.findByNameAndCategoryName(nameFilter, categoryName, pageNo);
		} else {
			items = itemService.findAll(pageNo);
		}
		return new ResponseEntity<List<Item>>((items.getContent()), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<ItemDto> create(@RequestBody ItemDto dto) {
		Item retVal = toItem.convert(dto);
		Item save = itemService.save(retVal);

		return new ResponseEntity<>(toDto.convert(save), HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		final Item item = itemService.findById(id);
		if (item == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		item.setCategory(null);
		itemService.deleteById(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Item> getOne(@PathVariable Long id) {
		Item i = itemService.findById(id);

		if (i != null) {
			return new ResponseEntity<>(i, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
