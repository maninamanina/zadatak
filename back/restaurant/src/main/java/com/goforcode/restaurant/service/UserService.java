package com.goforcode.restaurant.service;

import java.util.List;

import com.goforcode.restaurant.model.User;

public interface UserService {

	public List<User> findAll();

	public User save(User user);

	public User findByUsernameAndPassword(String username, String password);

}
