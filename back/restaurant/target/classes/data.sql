insert into category (name) values ('PASTA');
insert into category (name) values ('PIZZA');
insert into category (name) values ('COFFEE');
insert into category (name) values ('VINE');

insert into item (name, price, category_id) values ('Chicken Pasta', 100, 1);
insert into item (name, price, category_id) values ('Chicken Pizza', 150, 2);
insert into item (name, price, category_id) values ('Latte', 50, 3);
insert into item (name, price, category_id) values ('Moka', 50, 3);

insert into user (username, password) values ('user', 'user');